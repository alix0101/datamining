# encoding = 'utf-8'



import sklearn
import os
import numpy
import sys
import codecs
import pandas
import pickle
import joblib
from sklearn import ensemble



reload(sys)
sys.setdefaultencoding('utf8')
os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'
global_conf = sklearn.set_config(True)



# data pre-processing
# Reading the file from train/test csv data sheet, and get labels and data for classifier testing and
# training. If the training and testing data sheets are empty, then return empty labels and data.

def data_pre_process():
    test_f = codecs.open('../data/ads_test.csv', 'r')
    train_f = codecs.open('../data/ads_train.csv', 'r')

    test_labels = []
    train_labels = []

    if test_f is not None:
        test_dataset = numpy.loadtxt(test_f, dtype = 'str', skiprows = 1, delimiter= ',')
        shape = test_dataset.shape
        row_num = shape[0]
        col_num = shape[1]
        if col_num > 0:
            test_labels = test_dataset[:, 1]
            test_labels = numpy.asarray(test_labels, int)
            test_data = test_dataset[:, 2:col_num]
            test_data = numpy.array(test_data)
            test_data[test_data == 'NA'] = '0'
            test_data = numpy.asfarray(test_data, float)

    if train_f is not None:
        train_dataset = numpy.loadtxt(train_f,  dtype = 'str', skiprows = 1, delimiter= ',')
        shape = train_dataset.shape
        row_num = shape[0]
        col_num = shape[1]
        if col_num > 0:
            train_labels = train_dataset[:, 1]
            train_labels = numpy.asarray(train_labels, int)
            train_data = train_dataset[:, 2:col_num-1]
            train_data = numpy.array(train_data)
            train_data[train_data == 'NA'] = '0'
            train_data = numpy.asfarray(train_data, float)

    return train_data, train_labels, test_data, test_labels




def randomtree_classifier(train_data, train_labels):
    model = ensemble.RandomForestClassifier()
    model.n_estimators = 50
    model.max_depth = 8
    model.criterion = 'gini'
    model.class_weight = {0: 0.04}
    model.bootstrap = True
    model.max_features = 5
    model.n_classes_ = 2
    model.max_leaf_nodes = 45
    model.min_samples_split = 2
    if train_data is not None and train_labels is not None:
        model.fit(train_data, train_labels)
    joblib.dump(model, '../model/randomtree_classifier_model.joblib')
    return model



def load_model(model_name, path = None):
    model = None
    if path is None:
        model = joblib.load(model_name)
    else:
        model = joblib.load(os.path.join(path, model_name))
    return model

# calculate acc_value, precision_value, recall_value, f_score, auc_value
# values for testing models
def evaluate(true_label, predic_label, predict_prob_metrix = None):
    acc_value = ''
    precision_value = ''
    recall_value = ''
    f_score = ''
    auc_value = ''
    if true_label is not None and predic_label is not None:
        acc_value = sklearn.metrics.accuracy_score(true_label, predic_label)
        recall_value = sklearn.metrics.recall_score(true_label, predic_label)
        precision_value = sklearn.metrics.precision_score(true_label, predic_label)
        f_score = sklearn.metrics.f1_score(true_label, predic_label)
    if predict_prob_metrix is not None:
        auc_value = sklearn.metrics.roc_auc_score(true_label, predict_prob_metrix)
    return [acc_value, precision_value, recall_value, f_score, auc_value]


if __name__=='__main__':
    [train_data, train_labels, test_data, test_labels] = data_pre_process()
    model_randomtree = randomtree_classifier(train_data, train_labels)
    # model_randomtree = joblib.load('../model/randomtree_classifier_model.joblib')

    predic_labels = model_randomtree.predict(train_data)
    predict_prob = model_randomtree.predict_proba(train_data)

    score_value = evaluate(train_labels, predic_labels, predict_prob[:, 1])
    confusion_matrics = sklearn.metrics.confusion_matrix(train_labels, predic_labels)
    print score_value
    print confusion_matrics
    print model_randomtree.feature_importances_
